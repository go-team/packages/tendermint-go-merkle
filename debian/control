Source: tendermint-go-merkle
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>
Build-Depends:
 debhelper-compat (= 12),
 dh-golang,
 golang-any,
 golang-github-stretchr-testify-dev,
 golang-github-tendermint-go-common-dev,
 golang-github-tendermint-go-db-dev (>= 0.1~20170131~0git72f6dac),
 golang-github-tendermint-go-wire-dev,
 golang-golang-x-crypto-dev
Standards-Version: 3.9.8
Homepage: https://github.com/tendermint/go-merkle
Vcs-Browser: https://salsa.debian.org/go-team/packages/tendermint-go-merkle
Vcs-Git: https://salsa.debian.org/go-team/packages/tendermint-go-merkle.git
XS-Go-Import-Path: github.com/tendermint/go-merkle

Package: golang-github-tendermint-go-merkle-dev
Architecture: all
Depends:
 golang-github-tendermint-go-common-dev,
 golang-github-tendermint-go-db-dev,
 golang-github-tendermint-go-wire-dev,
 golang-golang-x-crypto-dev,
 ${misc:Depends},
 ${shlibs:Depends}
Description: Merkle-ized data structures with proofs
 This package provides two types of merkle trees:
  * IAVL+ Tree: A snapshottable (immutable) AVL+ tree for persistent
    data
  * A simple merkle tree for static dataIAVL+ tree; the purpose of
    this data structure is to provide persistent storage for
    key-value pairs (say to store account balances) such that a
    deterministic merkle root hash can be computed. The tree is
    balanced using a variant of the AVL algortihm so that all
    operations are O(log(n)).
 .
 This package provides a library used by Tendermint Core.
 .
 Tendermint Core is Byzantine Fault Tolerant (BFT) middleware
 that takes a state transition machine, written in any
 programming language, and replicates it on many machines.
